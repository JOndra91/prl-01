/*
 * File:   main-mes.cpp
 * Author: Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */

#include <mpi.h>
#include <iostream>
#include <fstream>

using namespace std;

#ifdef TIMERS
#include <ctime>
#endif

#define TAG_DISTRIBUTE 0
#define TAG_VALUE 2

#ifdef DEBUG
#define dprintf(args...) fprintf(stderr, args)
#else
#define dprintf(...)
#endif

class Node {

private:
    int nodeCount, numberCount;
    int nodeId, leftId, rightId, parentId;
    unsigned int value, leftValue, rightValue;

    bool bIsRoot;
    bool bIsLeaf;
    bool bHasValue;

    MPI_Status status;

    inline bool shouldReceiveLeft() {
        return leftId != -1 && leftValue == -1;
    }

    inline bool shoudlReceiveRight() {
        return rightId != -1 && rightValue == -1;
    }

public:
    Node(int _nodeId, int _nodeCount) {
        nodeCount = _nodeCount;
        nodeId = _nodeId;
        numberCount = (nodeCount + 1) >> 1;

        value = -1;
        leftValue = -1;
        rightValue = -1;

        bIsRoot = nodeId == 0;
        bIsLeaf = nodeId >= (numberCount - 1);
        bHasValue = true;

        if(bIsLeaf) {
            leftId = -1;
            rightId = -1;
        }
        else {
            leftId = (nodeId << 1) + 1;
            rightId = (nodeId << 1) + 2;
        }

        parentId = (nodeId - 1) >> 1;
    }

    inline bool isRoot() {
        return bIsRoot;
    }

    inline bool isLeaf() {
        return bIsLeaf;
    }

    void distributeNumbers(string numbersFiles) {
        ifstream numbers;
        unsigned int value;
        int nodeOffset = numberCount - 1;

        numbers.open(numbersFiles.c_str(), ios::binary);

        dprintf("Number count is %d\n", numberCount);

        for(int i = 0; i < numberCount; i++) {
            value = numbers.get();
            if(numbers.good()) {
                printf("%u ", value);
            }
            else {
                value = -1;
            }

            dprintf("Sending number %u to node %d\n", value, nodeOffset + i);
            MPI_Send(&value, 1, MPI_UNSIGNED, nodeOffset + i, TAG_DISTRIBUTE, MPI_COMM_WORLD);
        }

        printf("\n");

        numbers.close();
    }

    void recieveDistributedNumber() {
        MPI_Recv(&value, 1, MPI_UNSIGNED, 0, TAG_DISTRIBUTE, MPI_COMM_WORLD, &status);
        dprintf("Node %d received number %u\n", nodeId, value);
    }

    bool receiveValues() {
        if(shouldReceiveLeft()) {
            MPI_Recv(&leftValue, 1, MPI_UNSIGNED, leftId, TAG_VALUE, MPI_COMM_WORLD, &status);
            dprintf("Node %d recieved value %u from left node %d\n", nodeId, leftValue, leftId);
        }
        if(shoudlReceiveRight()) {
            MPI_Recv(&rightValue, 1, MPI_UNSIGNED, rightId, TAG_VALUE, MPI_COMM_WORLD, &status);
            dprintf("Node %d recieved value %u from right node %d\n", nodeId, rightValue, rightId);
        }

        // Disable empty subtree

        if(leftValue == -1) {
            leftId = -1;
        }

        if(rightValue == -1) {
            rightId = -1;
        }

        return rightValue != -1 || leftValue != -1;
    }

    void sendValue() {
        if(!isLeaf()) {
            receiveValues();

            value = getMin();
        }

        // Node doesn't have any value after it sends -1 to it's parent.
        bHasValue = value != -1;

        MPI_Send(&value, 1, MPI_UNSIGNED, parentId, TAG_VALUE, MPI_COMM_WORLD);
        dprintf("Node %d sent value %u to parent node %d\n", nodeId, value, parentId);
        value = -1;
    }

    inline unsigned int getMin() {
        // This is an unsigned comparison so -1 value is actually positive
        // number UINT_MAX which is always greater than any sorted value.
        unsigned int value;
        if(leftValue < rightValue) {
            value = leftValue;
            leftValue = -1;
        }
        else {
            value = rightValue;
            rightValue = -1;
        }

        return value;
    }

    inline bool canReceive() {
        return leftId != -1 || rightId != -1;
    }

    inline bool hasValue() {
        return bHasValue || canReceive();
    }
};

int main(int argc, char** argv) {

    int nodeCount;
    int nodeId;
    string numbersFile;

    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD,&nodeCount);
    MPI_Comm_rank(MPI_COMM_WORLD,&nodeId);

    Node n(nodeId, nodeCount);

    if(n.isRoot()) {
        if(argc >= 2) {
            numbersFile = string(argv[1]);
        }
        else {
            dprintf("Error: %s\n", "Expecting numbers file name as argument.");
            MPI_Abort(MPI_COMM_WORLD, 1);
        }
        n.distributeNumbers(numbersFile);
    }

#ifdef TIMERS
    struct timespec start;
    if(n.isRoot()) {
        clock_gettime(CLOCK_MONOTONIC, &start);
    }
#endif

    if(n.isLeaf()) {
        // All leaf nodes receive distributed numbers.
        n.recieveDistributedNumber();
    }

    if(n.isRoot()) {
        while(n.canReceive() && n.receiveValues()) {
            printf("%u\n", n.getMin());
        }
    }
    else {
        // Every node has to send it's value
        do {
            n.sendValue();
        } while(n.hasValue());
    }

    dprintf("Node %d has left.\n", nodeId);

#ifdef TIMERS
    struct timespec end, diff;
    double ddiff;
    if(n.isRoot()) {
        clock_gettime(CLOCK_MONOTONIC, &end);

        diff.tv_sec = end.tv_sec - start.tv_sec;
        diff.tv_nsec = end.tv_nsec - start.tv_nsec;

        if(diff.tv_nsec < 0) {
            diff.tv_sec -= 1;
            diff.tv_nsec += 1000000000;
        }

        // fprintf(stderr, "Sec: %lu, nSec: %lu, mSec: %f\n", start.tv_sec, start.tv_nsec, (start.tv_sec * 1e3f) + (start.tv_nsec / 1e6f));
        // fprintf(stderr, "Sec: %lu, nSec: %lu, mSec: %f\n", end.tv_sec, end.tv_nsec, (end.tv_sec * 1e3f) + (end.tv_nsec / 1e6f));
        // fprintf(stderr, "Sec: %lu, nSec: %lu, mSec: %f\n", diff.tv_sec, diff.tv_nsec, (diff.tv_sec * 1e3f) + (diff.tv_nsec / 1e6f));

        // Miliseconds
        ddiff = ((diff.tv_sec * 1e3f) + (diff.tv_nsec / 1e6f));

        fprintf(stderr, "%f\n", ddiff);
    }
#endif

    MPI_Finalize();

    return 0;
}
