#!/usr/bin/env runhaskell

import System.Environment
import Text.Read
import Data.Maybe

main :: IO ()
main = (average . lines <$> (readFile . head =<< getArgs)) >>= print

average :: [String] -> Float
average list = floats !! (length floats `quot` 2)
  where
    floats = catMaybes (readMaybe <$> list)
