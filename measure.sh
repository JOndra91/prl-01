#!/bin/bash

set -e

declare -r resultsDir="results_`date +'%F-%T'`"
declare -r name='mes'
declare -r cppFile="$name.cpp"
declare -r executable="$name"
declare numbersFile='measure-numbers'
declare customNumbers=false

generateNumbers() {
    local -ir numberCount=$1; shift

    dd if=/dev/urandom bs=1 count=$numberCount of="$numbersFile" 2>/dev/null 1>&2
}

finish() {

    printf 'count,median,average\n' > "$resultsDir/results"

    for f in "$resultsDir"/measure_*; do

        local -i num=`sed -r 's/^.*\/measure_0*//' <<< "$f"`

        local average="`./average.hs "$f"`"
        local median="`./median.hs "$f"`"

        printf '%d,%s,%s\n' \
            $num $median $average >> "$resultsDir/results"
    done

    $customNumbers || rm -f "$numbersFile"
    rm -f "$executable"
}

main() {
    trap finish EXIT

    export OMPI_CXXFLAGS='-DTIMERS'
    mpic++ -o "$executable" "$cppFile"

    mkdir "$resultsDir"

    ulimit -n 4096

    while true; do
        for i in {3..99..3}; do

            local -i valueCount=$i
            local -i cpuCount="`php -r "printf(\\\"%d\\\\n\\\", 2*pow(2,ceil(log($valueCount)/log(2))) - 1);"`"

            local zeroI=`printf '%02d' $i`

            printf 'Running with %d values and %d processors.\n' $valueCount $cpuCount

            generateNumbers $valueCount
            mpirun -np $cpuCount "$executable" "$numbersFile" \
                2>> "${resultsDir}/measure_${zeroI}"
        done
    done

}

main "$@"
