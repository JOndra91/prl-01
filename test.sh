#!/bin/bash

set -e

declare -r name='mes'
declare -r cppFile="$name.cpp"
declare -r executable="$name"
declare numbersFile='numbers'
declare customNumbers=false

generateNumbers() {
    local -ir numberCount=$1; shift

    dd if=/dev/urandom bs=1 count=$numberCount of="$numbersFile" 2>/dev/null 1>&2
}

cleanup() {
    $customNumbers || rm -f "$numbersFile"
    rm -f "$executable"
}

main() {
    local -i valueCount="$1"; shift
    local -i cpuCount="$1"; shift

    local -a cxxFlags=();

    local arg;
    while (( $# )); do
        arg="$1"; shift

        case "$arg" in
            --numbers)
                customNumbers=true
                numbersFile="$1"; shift
                ;;
            --debug)
                cxxFlags+=('-DDEBUG')
                ;;
            --timers)
                cxxFlags+=('-DTIMERS')
                ;;
            *)
                printf '%s: %s' "$arg" 'Unexpected argument' 1>&2
                exit 1
                ;;
        esac
    done

    trap cleanup EXIT

    if ! $customNumbers || [[ ! -f "$numbersFile" ]]; then
        generateNumbers $valueCount
    fi


    local -a mpiFlags=()
    if [[ "`uname --nodename`" == 'merlin.fit.vutbr.cz' ]]; then
        mpiFlags+=(
            --prefix /usr/local/share/OpenMPI
        )
    fi

    export OMPI_CXXFLAGS="${cxxFlags[@]}"
    mpic++ "${mpiFlags[@]}" -o "$executable" "$cppFile"
    mpirun "${mpiFlags[@]}" -np $cpuCount "$executable" "$numbersFile"

}

main "$@"
